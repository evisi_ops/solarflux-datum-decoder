# SolarFlux Datum Thingsboard Decoder

This project contains a Thingsboard PE uplink data converter that is designed to convert [SolarFlux
datum messages][solarflux-datum] into Thingsboad JSON messages.

SolarFlux datum messages are [CBOR][cbor] encoded maps, which can be easily transformed into the
JSON Thingsboard requires, once it has been decoded from CBOR. This data converter adapts the CBOR
decoding functions from the [cbor-sync][cbor-sync] project.

For example, here's a sample SolarFlux datum message, in JSON:

```json
{
	"sourceId": "/TXLD/SITE1/BULD1/METR/1",
	"watts": 129039,
	"wattHours": 108752740,
	"created": 1576051429002
}
```

Assuming this arrived on a SolarFlux MQTT topic `user/321/node/123/datum/0/TXLD/SITE1/BULD1/METR/1` the
data converter will transform that message to this JSON:

```json
{
	"deviceName": "METR1",
	"deviceType": "METR",
	"customerName": "TXLD",
	"groupName": "SITE1",
	"attributes": { "nodeId": 123 },
	"telemetry": {
		"ts": 1576051429002,
		"values": { "metr_watts": 129039, "metr_wattHours": 108752740 }
	}
}
```

# Deploying in Thingsboard

The [src/index.js](src/index.js) file contains the source for the data converter. You must copy/paste
the contents of that file, **without the leading `function {` and trailing `}` lines**. That is,
**omit** the first 3 lines:

```javascript
// NOTE that for TB integration, the first "export ... {" line and final "}" line
//      are NOT used; only the BODY of this function should be copy/pasted into TB.
export default function Decoder(payload, metadata) {
```

and final line:

```
}
```

These lines are used to make testing the code in this project easier.

# Testing

You can run the unit tests after installing the development packages with `npm` or `yarn` by
running the `test` script. For example:

```sh
# first time, install dependencies
yarn install

# run tests
yarn run test
```

[cbor]: https://en.wikipedia.org/wiki/CBOR
[cbor-sync]: https://github.com/ARMmbed/cbor-sync
[solarflux-datum]: https://github.com/SolarNetwork/solarnetwork/wiki/SolarFlux-API#datum-message-format
