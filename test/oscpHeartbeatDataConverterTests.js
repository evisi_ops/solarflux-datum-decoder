import test from "ava";

import decode from "../src/oscpHeartbeatDataConverter.js";

test("placeholder", t => {
	t.pass("Nice one!");
});

test("decode:http:oscp:heartbeat:1", t => {
	const payload = new Uint8Array(
		/*
		{
			"offline_mode_at" : "2022-11-08T06:15:22Z",
		}
		*/
		Buffer.from(
			"7b226f66666c696e655f6d6f64655f617422203a2022323032322d31312d30385430363a31353a32325a227d0a",
			"hex"
		)
	);
	const res = decode(payload, {
		deviceName: "myDevice",
		customerName: "myCustomer",
		nodeId: "123"
	});
	t.deepEqual(res, {
		deviceName: "myDevice",
		deviceType: "pzemMtr",
		customerName: "myCustomer",
		groupName: "transformers",
		attributes: {
			nodeId: 123,
			fpOscpExpiry: "2022-11-08T06:15:22Z"
		}
	});
});
