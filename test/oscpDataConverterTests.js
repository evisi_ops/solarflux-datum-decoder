import test from "ava";

import decode from "../src/oscpDataConverter.js";

test("placeholder", t => {
	t.pass("Nice one!");
});

test("decode:oscp:heartbeat:1", t => {
	const payload = new Uint8Array(
		/*
		{
			"created" : 1667880926040,
			"created_date_" : "2022-11-08T17:15:26.04+13:00[Pacific/Auckland]",
			"expires" : "2022-11-08 06:15:22Z",
			"nodeId" : 612,
			"sourceId" : "/oscp/nzbus/pnmr_trans_1/cp/Vector/NZ-AUK_0001/h"
		}
		*/
		Buffer.from(
			"BF67637265617465641B0000018455733B58666E6F6465496419026468736F75726365496478382F6F7363702F6E7A6275732F706E6D745F7472616E735F312F63702F566563746F722F4E5A2D41554B5F303030312F486561727462656174676578706972657374323032322D31312D30382030363A31353A32325AFF",
			"hex"
		)
	);
	const res = decode(payload, {
		topic: "user/1/node/2/datum/0/oscp/NZBus/pnmr_trans_1/cp/Vector/NZ-AUK_0001/h"
	});
	t.deepEqual(res, {
		deviceName: "pnmr_trans_1",
		deviceType: "pzemMtr",
		customerName: "NZBus",
		groupName: "transformers",
		attributes: {
			nodeId: 2, // note how comes from MQTT topic, not datum
			cpOscpExpiry: "2022-11-08 06:15:22Z"
		}
	});
});

test("decode:oscp:updategroupcapacityforecast:1", t => {
	const payload = new Uint8Array(
		/*
		{
			"amount" : 1000.0,
			"created" : 1667887099000,
			"created_date_" : "2022-11-08T18:58:19+13:00[Pacific/Auckland]",
			"duration" : 0,
			"forecastIdentifier" : "tu26s1KhMlNUeoG3wVP77tn5JcdiK2NL",
			"nodeId" : 612,
			"sourceId" : "/oscp/nzbus/pnmr_trans_1/cp/Vector/NZ-AUK_0001/ugcf/c",
			"unit" : "kW"
		}
		*/
		Buffer.from(
			"BF67637265617465641B0000018455D16C78666E6F6465496419026468736F75726365496478352F6F7363702F6E7A6275732F706E6D725F7472616E735F312F63702F566563746F722F4E5A2D41554B5F303030312F756763662F63686475726174696F6E0066616D6F756E74C4822019271072666F7265636173744964656E74696669657278207475323673314B684D6C4E55656F47337756503737746E354A6364694B324E4C64756E6974626B57FF",
			"hex"
		)
	);
	const res = decode(payload, {
		topic: "user/1/node/2/datum/0/oscp/NZBus/pnmr_trans_1/cp/Vector/NZ-AUK_0001/ugcf/c"
	});
	t.deepEqual(res, {
		deviceName: "pnmr_trans_1",
		deviceType: "pzemMtr",
		customerName: "NZBus",
		groupName: "transformers",
		attributes: {
			nodeId: 2 // note how comes from MQTT topic, not datum
		},
		telemetry: {
			ts: 1667887099000,
			values: {
				endTime: 1667887099000,
				value: 1000.0,
				unit: "kW",
				forecastIdentifier: "tu26s1KhMlNUeoG3wVP77tn5JcdiK2NL",
				forecastType: "c"
			}
		}
	});
});

test("decode:oscp:updategroupcapacityforecast:2", t => {
	const payload = new Uint8Array(
		/*
		{
			"amount" : 1000.0,
			"created" : 1667929550000,
			"created_date_" : "2022-11-09T06:45:50+13:00[Pacific/Auckland]",
			"duration" : 1,
			"forecastIdentifier" : "lwreEieKLBpqGBEojCK48iLthqE4I4UE",
			"nodeId" : 612,
			"sourceId" : "/oscp/NZBus/pnmr_trans_1/cp/Vector/NZ-AUK_0001/ugcf/c",
			"unit" : "kW"
		}
		*/
		Buffer.from(
			"BF67637265617465641B0000018458592CB0666E6F6465496419026468736F75726365496478352F6F7363702F4E5A4275732F706E6D725F7472616E735F312F63702F566563746F722F4E5A2D41554B5F303030312F756763662F63686475726174696F6E0166616D6F756E74C4822019271072666F7265636173744964656E74696669657278206C7772654569654B4C4270714742456F6A434B3438694C74687145344934554564756E6974626B57FF",
			"hex"
		)
	);
	const res = decode(payload, {
		topic: "user/1/node/2/datum/0/oscp/NZBus/pnmr_trans_1/cp/Vector/NZ-AUK_0001/ugcf/c"
	});
	t.deepEqual(res, {
		deviceName: "pnmr_trans_1",
		deviceType: "pzemMtr",
		customerName: "NZBus",
		groupName: "transformers",
		attributes: {
			nodeId: 2 // note how comes from MQTT topic, not datum
		},
		telemetry: {
			ts: 1667929550000,
			values: {
				endTime: 1667929551000, // is created + duration
				value: 1000.0,
				unit: "kW",
				forecastIdentifier: "lwreEieKLBpqGBEojCK48iLthqE4I4UE",
				forecastType: "c"
			}
		}
	});
});

const plcExampleMetadata = {
	customer: "NZBus",
	chgrDeviceType: "Sinexcel",
	chgrDeviceGroup: "charger",
	transDeviceName: "pnmr_trans_1",
	transDeviceType: "pzemMtr",
	transDeviceGroup: "transformer",
	psuDeviceName: "pnmr_trans_1",
	psuDeviceType: "pzemMtr",
	psuDeviceGroup: "transformer"
};

test("decode:oscp:plc:charger:1", t => {
	const payload = new Uint8Array(
		/*
		{
			"created" : 1667887099000,
			"cbStatus": 0,
			"cbFault": 0,
			"cbPower": 1,
			"lmpAvail": 1,
			"lmpUnavail": 0
		}
		*/
		Buffer.from(
			"a667637265617465641b0000018455d16c78686362537461747573006763624661756c7400676362506f77657201686c6d70417661696c016a6c6d70556e617661696c00",
			"hex"
		)
	);
	const res = decode(
		payload,
		Object.assign({ topic: "user/1/node/2/datum/0/nzbus/pnmr/chgr/1" }, plcExampleMetadata)
	);
	t.deepEqual(res, {
		deviceName: "pnmrChgr1",
		deviceType: "Sinexcel",
		customerName: "NZBus",
		groupName: "charger",
		attributes: {
			nodeId: 2 // note how comes from MQTT topic, not datum
		},
		telemetry: {
			ts: 1667887099000,
			values: {
				cbStatus: 0,
				cbFault: 0,
				cbPower: 1,
				lmpAvail: 1,
				lmpUnavail: 0
			}
		}
	});
});

test("decode:oscp:plc:transformer:1", t => {
	const payload = new Uint8Array(
		/*
		{
			"created" : 1667887099000,
			"watts": 123,
			"wattHours": 3456789,
			"wattsMax": 234,
			"wattsMaxN1": 345,
			"wattsMaxN2": 456
		}
		*/
		Buffer.from(
			"a667637265617465641b0000018455d16c78657761747473187b6977617474486f7572731a0034bf156877617474734d617818ea6a77617474734d61784e311901596a77617474734d61784e321901c8",
			"hex"
		)
	);
	const res = decode(
		payload,
		Object.assign({ topic: "user/1/node/2/datum/0/nzbus/pnmr/trans/1" }, plcExampleMetadata)
	);
	t.deepEqual(res, {
		deviceName: "pnmr_trans_1",
		deviceType: "pzemMtr",
		customerName: "NZBus",
		groupName: "transformer",
		attributes: {
			nodeId: 2 // note how comes from MQTT topic, not datum
		},
		telemetry: {
			ts: 1667887099000,
			values: {
				watts: 123,
				wattHours: 3456789,
				wattsMax: 234,
				wattsMaxN1: 345,
				wattsMaxN2: 456
			}
		}
	});
});

test("decode:oscp:plc:transformer:2", t => {
	const payload = new Uint8Array(
		/*
		{
			"created" : 1667887099000,
			"watts": 123,
			"_v2" : 1,
			"_another": "boo"
		}
		*/
		Buffer.from(
			"a467637265617465641b0000018455d16c78657761747473187b635f763201685f616e6f7468657263626f6f",
			"hex"
		)
	);
	const res = decode(
		payload,
		Object.assign({ topic: "user/1/node/2/datum/0/nzbus/pnmr/trans/1" }, plcExampleMetadata)
	);
	t.deepEqual(res, {
		deviceName: "pnmr_trans_1",
		deviceType: "pzemMtr",
		customerName: "NZBus",
		groupName: "transformer",
		attributes: {
			nodeId: 2 // note how comes from MQTT topic, not datum
		},
		telemetry: {
			ts: 1667887099000,
			values: {
				watts: 123
				// note how _v2 and _another properties are removed
			}
		}
	});
});

test("decode:oscp:plc:psu:1", t => {
	const payload = new Uint8Array(
		/*
		{
			"created" : 1667887099000,
			"fail": "0",
			"low": "0"
		}
		*/
		Buffer.from("a367637265617465641b0000018455d16c78646661696c6130636c6f776130", "hex")
	);
	const res = decode(
		payload,
		Object.assign({ topic: "user/1/node/2/datum/0/nzbus/pnmr/psu" }, plcExampleMetadata)
	);
	t.deepEqual(res, {
		deviceName: "pnmr_trans_1",
		deviceType: "pzemMtr",
		customerName: "NZBus",
		groupName: "transformer",
		attributes: {
			nodeId: 2 // note how comes from MQTT topic, not datum
		},
		telemetry: {
			ts: 1667887099000,
			values: {
				fail: "0",
				low: "0"
			}
		}
	});
});
