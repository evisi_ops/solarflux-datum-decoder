import test from "ava";

import decode from "../src/oscpUpdateGroupCapacityForecastDataConverter.js";

test("placeholder", t => {
	t.pass("Nice one!");
});

test("decode:http:oscp:updategroupcapacityforecast:1", t => {
	const payload = new Uint8Array(
		/*
{
	"group_id" : "abc123",
	"type" : "CONSUMPTION",
	"forecasted_blocks" : [
		{
			"capacity" : 123.456,
			"phase" : "ALL",
			"unit" : "KW",
			"start_time" : "2022-10-07T00:00:00Z",
			"end_time" : "2022-10-07T00:15:00Z"
		},
		{
			"capacity" : 234.567,
			"phase" : "ALL",
			"unit" : "KW",
			"start_time" : "2022-10-07T00:15:00Z",
			"end_time" : "2022-10-07T00:30:00Z"
		},
		{
			"capacity" : 345.678,
			"phase" : "ALL",
			"unit" : "KW",
			"start_time" : "2022-10-07T00:30:00Z",
			"end_time" : "2022-10-07T00:45:00Z"
		},
		{
			"capacity" : 456.789,
			"phase" : "ALL",
			"unit" : "KW",
			"start_time" : "2022-10-07T00:45:00Z",
			"end_time" : "2022-10-07T01:00:00Z"
		}
	]
}

Example metadata:

{
	"customerName" "myCustomer",
	"deviceName": "myDevice",
	"nodeId": "612",
}

		*/
		Buffer.from(
			"7b0a092267726f75705f696422203a2022616263313233222c0a09227479706522203a2022434f4e53554d5054494f4e222c0a0922666f72656361737465645f626c6f636b7322203a205b0a09097b0a09090922636170616369747922203a203132332e3435362c0a09090922706861736522203a2022414c4c222c0a09090922756e697422203a20224b57222c0a0909092273746172745f74696d6522203a2022323032322d31302d30375430303a30303a30305a222c0a09090922656e645f74696d6522203a2022323032322d31302d30375430303a31353a30305a220a09097d2c0a09097b0a09090922636170616369747922203a203233342e3536372c0a09090922706861736522203a2022414c4c222c0a09090922756e697422203a20224b57222c0a0909092273746172745f74696d6522203a2022323032322d31302d30375430303a31353a30305a222c0a09090922656e645f74696d6522203a2022323032322d31302d30375430303a33303a30305a220a09097d2c0a09097b0a09090922636170616369747922203a203334352e3637382c0a09090922706861736522203a2022414c4c222c0a09090922756e697422203a20224b57222c0a0909092273746172745f74696d6522203a2022323032322d31302d30375430303a33303a30305a222c0a09090922656e645f74696d6522203a2022323032322d31302d30375430303a34353a30305a220a09097d2c0a09097b0a09090922636170616369747922203a203435362e3738392c0a09090922706861736522203a2022414c4c222c0a09090922756e697422203a20224b57222c0a0909092273746172745f74696d6522203a2022323032322d31302d30375430303a34353a30305a222c0a09090922656e645f74696d6522203a2022323032322d31302d30375430313a30303a30305a220a09097d0a095d0a7d",
			"hex"
		)
	);
	const res = decode(payload, {
		deviceName: "myDevice",
		customerName: "myCustomer",
		nodeId: "123",
		"Header:x-request-id": "ihyUwTqfVD8qsps10L8GXQUBwNBvkN08"
	});
	t.like(res, {
		deviceName: "myDevice",
		deviceType: "pzemMtr",
		customerName: "myCustomer",
		groupName: "transformers",
		attributes: {
			nodeId: 123
		},
		telemetry: {
			// ignore ts
			values: {
				oscpCapProfileCons: [
					{
						value: 123.456,
						unit: "KW",
						startTime: "2022-10-07T00:00:00Z",
						endTime: "2022-10-07T00:15:00Z",
						forecastIdentifier: "ihyUwTqfVD8qsps10L8GXQUBwNBvkN08"
					},
					{
						value: 234.567,
						unit: "KW",
						startTime: "2022-10-07T00:15:00Z",
						endTime: "2022-10-07T00:30:00Z",
						forecastIdentifier: "ihyUwTqfVD8qsps10L8GXQUBwNBvkN08"
					},
					{
						value: 345.678,
						unit: "KW",
						startTime: "2022-10-07T00:30:00Z",
						endTime: "2022-10-07T00:45:00Z",
						forecastIdentifier: "ihyUwTqfVD8qsps10L8GXQUBwNBvkN08"
					},
					{
						value: 456.789,
						unit: "KW",
						startTime: "2022-10-07T00:45:00Z",
						endTime: "2022-10-07T01:00:00Z",
						forecastIdentifier: "ihyUwTqfVD8qsps10L8GXQUBwNBvkN08"
					}
				]
			}
		}
	});
});
