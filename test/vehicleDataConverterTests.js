import test from "ava";

import decode from "../src/vehicleDataConverter.js";

test("placeholder", t => {
	t.pass("Nice one!");
});

const payload_1 = [
	191,
	104,
	115,
	111,
	117,
	114,
	99,
	101,
	73,
	100,
	118,
	47,
	84,
	82,
	65,
	78,
	90,
	47,
	66,
	85,
	83,
	47,
	82,
	79,
	78,
	71,
	49,
	47,
	71,
	80,
	83,
	47,
	49,
	101,
	119,
	97,
	116,
	116,
	115,
	0,
	105,
	119,
	97,
	116,
	116,
	72,
	111,
	117,
	114,
	115,
	26,
	6,
	123,
	111,
	100,
	103,
	99,
	114,
	101,
	97,
	116,
	101,
	100,
	27,
	0,
	0,
	1,
	110,
	243,
	252,
	78,
	138,
	255
];

test("decode:no_metadata", t => {
	const res = decode(payload_1);
	t.deepEqual(res, {
		deviceName: "RONG1",
		deviceType: "BUS",
		customerName: "TRANZ",
		groupName: "vehicles",
		telemetry: { ts: 1576051429002, values: { gps_1_watts: 0, gps_1_wattHours: 108752740 } }
	});
});

test("decode:with_metadata:topic", t => {
	const res = decode(payload_1, { topic: "user/1/node/2/datum/0/FOO/BAR/BIM/BAM/1" });
	t.deepEqual(res, {
		deviceName: "BIM",
		deviceType: "BAR",
		customerName: "FOO",
		groupName: "All",
		attributes: { nodeId: 2 },
		telemetry: {
			ts: 1576051429002,
			values: { watts: 0, wattHours: 108752740 }
		}
	});
});

test("decode:gps", t => {
	const res = decode(payload_1, { topic: "user/1/node/2/datum/0/FOO/BAR/BIM/GPS/1" });
	t.deepEqual(res, {
		deviceName: "BIM",
		deviceType: "BAR",
		customerName: "FOO",
		groupName: "All",
		attributes: { nodeId: 2 },
		telemetry: {
			ts: 1576051429002,
			values: { gps_1_watts: 0, gps_1_wattHours: 108752740 }
		}
	});
});

test("decode:os_stats", t => {
	const payload = new Uint8Array(
		Buffer.from(
			"bf6e7379735f6c6f61645f31356d696ec4820207667379735f7570c482011a001ca6b4726e65745f62797465735f6f75745f70707030c482001a00a4f5c17072616d5f757365645f70657263656e74c48200183067637265617465641b0000016f01eb9539686370755f69646c65c482001863686370755f74656d70c4820319c520716e65745f62797465735f696e5f70707030c482001a001d8c9fff",
			"hex"
		)
	);
	const res = decode(payload, { topic: "user/1/node/2/datum/0/FOO/BAR/BIM/OS/1" });
	t.deepEqual(res, {
		deviceName: "BIM",
		deviceType: "BAR",
		customerName: "FOO",
		groupName: "All",
		attributes: { nodeId: 2 },
		telemetry: {
			ts: 1576285214009,
			values: {
				os_1_cpu_idle: 99,
				os_1_cpu_temp: 50464000,
				os_1_net_bytes_in_ppp0: 1936543,
				os_1_net_bytes_out_ppp0: 10810817,
				os_1_ram_used_percent: 48,
				os_1_sys_load_15min: 700,
				os_1_sys_up: 18776840
			}
		}
	});
});

test("decode:bat:1", t => {
	const payload = new Uint8Array(
		Buffer.from(
			"bf6e6d696e4d6f6e6f6d657254656d70c482001a0601fff367637265617465641b0000016f0c5f463063736f63c4820319020c6e6d617843656c6c566f6c74616765c482023a00015e2367766f6c74616765c482011917d86c636861726765456e657267791a009eb9846763757272656e74c48201086e6d61784d6f6e6f6d657254656d70c482001a3c0002186c6f7574707574456e657267791a00b65f806e6d696e43656c6c566f6c74616765c4820139230370706f7349736f526573697374616e63651a019b6080706e656749736f526573697374616e63651a019b60806b726567656e456e657267791a00516a26ff",
			"hex"
		)
	);
	const res = decode(payload, { topic: "user/1/node/2/datum/0/FOO/BAR/BIM/BAT/1" });
	t.deepEqual(res, {
		deviceName: "BIM",
		deviceType: "BAR",
		customerName: "FOO",
		groupName: "All",
		attributes: { nodeId: 2 },
		telemetry: {
			ts: 1576460568112,
			values: {
				chargeEnergy: 10402180,
				current: 80,
				maxCellVoltage: -8963600,
				maxMonomerTemp: 1006633496,
				minCellVoltage: -89640,
				minMonomerTemp: 100794355,
				negIsoResistance: 26960000,
				outputEnergy: 11952000,
				posIsoResistance: 26960000,
				regenEnergy: 5335590,
				soc: 524000,
				voltage: 61040
			}
		}
	});
});

test("decode:bat:1:A", t => {
	const payload = new Uint8Array(
		Buffer.from(
			"A363736F63C482221902416763757272656E74C482223A000790E16C636861726765456E6572677919AA64",
			"hex"
		)
	);
	const res = decode(payload, { topic: "user/1/node/2/datum/0/FOO/BAR/BIM/BAM/1" });
	t.deepEqual(res, {
		deviceName: "BIM",
		deviceType: "BAR",
		customerName: "FOO",
		groupName: "All",
		attributes: { nodeId: 2 },
		telemetry: {
			values: {
				soc: 0.577,
				current: -495.842,
				chargeEnergy: 43620
			}
		}
	});
});

test("decode:accel:1", t => {
	const payload = new Uint8Array(
		Buffer.from(
			"BF68736F757263654964772F5452414E5A2F4255532F415554302F414343454C2F31707A5F617869735F616363656C5F6D696EC482223903D66C785F617869735F616363656CC482220E70795F617869735F616363656C5F6D6178C48222382F67637265617465641B00000174618C294470785F617869735F616363656C5F6D6178C48222181C70795F617869735F616363656C5F6D696EC48222384B707A5F617869735F616363656C5F6D6178C482223903B66C795F617869735F616363656CC48222383D6C7A5F617869735F616363656CC48221386070785F617869735F616363656C5F6D696EC4822206625F7602FF",
			"hex"
		)
	);
	const res = decode(payload, { topic: "user/1/node/2/datum/0/TRANZ/BUS/AUT0/ACCEL/1" });
	t.deepEqual(res, {
		deviceName: "AUT0",
		deviceType: "BUS",
		customerName: "TRANZ",
		groupName: "vehicles",
		attributes: { nodeId: 2 },
		telemetry: {
			ts: 1599364409668,
			values: {
				x_axis_accel: 0.014,
				x_axis_accel_max: 0.028,
				x_axis_accel_min: 0.006,
				y_axis_accel: -0.062,
				y_axis_accel_max: -0.048,
				y_axis_accel_min: -0.076,
				z_axis_accel: -0.97,
				z_axis_accel_max: -0.9510000000000001,
				z_axis_accel_min: -0.983
			}
		}
	});
});
