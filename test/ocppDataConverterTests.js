import test from "ava";

import decode from "../src/ocppDataConverter.js";

test("placeholder", t => {
	t.pass("Nice one!");
});

test("decode:ocpp:status:1", t => {
	const payload = new Uint8Array(
		Buffer.from(
			"a667637265617465647818323032302d30392d32352030343a31313a30302e3839395a666e6f646549641901c168736f75726365496478245452414e5a2f6f6370702f63702f76656566696c2d3132353738322f312f737461747573696c6f63616c446174656a323032302d30392d3235696c6f63616c54696d656531363a31316673746174757369417661696c61626c65",
			"hex"
		)
	);
	const res = decode(payload, {
		topic: "user/1/node/2/datum/0/TRANZ/ocpp/cp/veefil-125782/1/status"
	});
	t.deepEqual(res, {
		deviceName: "veefil-125782_CONN_1",
		deviceType: "evconnector",
		customerName: "TRANZ",
		groupName: "evchargers",
		attributes: {
			nodeId: 2,
			chargerIdentifier: "veefil-125782"
		},
		telemetry: {
			ts: "2020-09-25 04:11:00.899Z",
			values: {
				ocpp_localDate: "2020-09-25",
				ocpp_localTime: "16:11",
				ocpp_nodeId: 449,
				ocpp_status: "Available"
			}
		}
	});
});

test("decode:ocpp:outlet:1", t => {
	const payload = new Uint8Array(
		Buffer.from(
			"ae67637265617465647818323032302d30392d32352030343a30303a33382e3636325a666e6f646549641901c168736f75726365496478245452414e5a2f6f6370702f63702f76656566696c2d3132353738322f322f4f75746c6574696c6f63616c446174656a323032302d30392d3235696c6f63616c54696d656531363a303063736f631837657761747473fb40e58406666666666763757272656e7418766e63757272656e744f66666572656418766977617474486f75727319135765746f6b656e684136383536443535686475726174696f6e1901a469656e64526561736f6e67556e6b6e6f776e6973657373696f6e4964782437666637646537322d316639662d343638662d616566652d666263323839333235626635",
			"hex"
		)
	);
	const res = decode(payload, {
		topic: "user/1/node/2/datum/0/TRANZ/ocpp/cp/veefil-125782/2/Outlet"
	});
	t.deepEqual(res, {
		deviceName: "veefil-125782_CONN_2",
		deviceType: "evconnector",
		customerName: "TRANZ",
		groupName: "evchargers",
		attributes: {
			nodeId: 2,
			chargerIdentifier: "veefil-125782"
		},
		telemetry: {
			ts: "2020-09-25 04:00:38.662Z",
			values: {
				ocpp_current: 118,
				ocpp_currentOffered: 118,
				ocpp_duration: 420,
				ocpp_endReason: "Unknown",
				ocpp_localDate: "2020-09-25",
				ocpp_localTime: "16:00",
				ocpp_nodeId: 449,
				ocpp_sessionId: "7ff7de72-1f9f-468f-aefe-fbc289325bf5",
				ocpp_soc: 55,
				ocpp_token: "A6856D55",
				ocpp_wattHours: 4951,
				ocpp_watts: 44064.2
			}
		}
	});
});

test("decode:ocpp:outlet:phase:L1", t => {
	const payload = new Uint8Array(
		Buffer.from(
			"ae67637265617465647818323032302d30392d32352030343a30303a33382e3636325a666e6f646549641901c168736f75726365496478245452414e5a2f6f6370702f63702f76656566696c2d3132353738322f322f4f75746c6574696c6f63616c446174656a323032302d30392d3235696c6f63616c54696d656531363a303063736f631837657761747473fb40e58406666666666763757272656e7418766e63757272656e744f66666572656418766977617474486f75727319135765746f6b656e684136383536443535686475726174696f6e1901a469656e64526561736f6e67556e6b6e6f776e6973657373696f6e4964782437666637646537322d316639662d343638662d616566652d666263323839333235626635",
			"hex"
		)
	);
	const res = decode(payload, {
		topic: "user/1/node/2/datum/0/TRANZ/ocpp/cp/veefil-125782/2/Outlet/L1"
	});
	t.deepEqual(res, {
		deviceName: "veefil-125782_CONN_2",
		deviceType: "evconnector",
		customerName: "TRANZ",
		groupName: "evchargers",
		attributes: {
			nodeId: 2,
			chargerIdentifier: "veefil-125782"
		},
		telemetry: {
			ts: "2020-09-25 04:00:38.662Z",
			values: {
				ocpp_current: 118,
				ocpp_currentOffered: 118,
				ocpp_duration: 420,
				ocpp_endReason: "Unknown",
				ocpp_localDate: "2020-09-25",
				ocpp_localTime: "16:00",
				ocpp_nodeId: 449,
				ocpp_sessionId: "7ff7de72-1f9f-468f-aefe-fbc289325bf5",
				ocpp_soc: 55,
				ocpp_token: "A6856D55",
				ocpp_wattHours: 4951,
				ocpp_watts: 44064.2
			}
		}
	});
});
