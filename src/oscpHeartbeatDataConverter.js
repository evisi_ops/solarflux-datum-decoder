/* HTTP OSCP Heartbeat input converter

NOTE that for TB integration, the first "export ... {" line and final "}" line
are NOT used; only the BODY of this function should be copy/pasted into TB.

Example input:

{
	"offline_mode_at" : "2022-11-08T06:15:22Z",
}

*/

export default function Decoder(payload, metadata) {
	"use strict";

	var msg = JSON.parse(String.fromCharCode.apply(String, payload));

	var result = {
		deviceType: "pzemMtr",
		groupName: "transformers",
		deviceName: metadata.deviceName,
		customerName: metadata.customerName,
		attributes: {
			nodeId: Number(metadata.nodeId),
			fpOscpExpiry: msg.offline_mode_at
		}
	};

	return result;
}
