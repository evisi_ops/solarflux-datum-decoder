/* HTTP OSCP UpdateGroupCapacityForecast input converter

NOTE that for TB integration, the first "export ... {" line and final "}" line
are NOT used; only the BODY of this function should be copy/pasted into TB.

Example payload:

{
    "group_id": "NZ-AUK_0001",
    "type": "CONSUMPTION",
    "forecasted_blocks": [
		{
			"capacity": 1000.0,
			"phase": "ALL",
			"unit": "KW",
			"start_time": "2022-11-12T05:58:23.000Z",
			"end_time": "2022-11-12T05:58:24.000Z"
    	}
	]
}

Example metadata:

{
	"customerName" "myCustomer",
	"deviceName": "myDevice",
	"nodeId": "612",
    "Header:accept-encoding": "gzip,deflate",
    "Header:content-type": "application/json",
    "Header:content-length": "193",
    "Header:user-agent": "Apache-HttpClient/4.5.13 (Java/17.0.5)",
    "integrationName": "Vector OSCP updateForecast",
    "Header:authorization": "Token IN4sqc...",
    "Header:x-forwarded-port": "443",
    "Header:host": "nzbus.evisi.co",
    "Header:accept": "application/json, application/cbor, application/*+json",
    "Header:x-forwarded-for": "34.208.246.0",
    "Header:x-request-id": "ihyUwTqfVD8qsps10L8GXQUBwNBvkN08",
    "Header:x-forwarded-proto": "https"
}
*/

export default function Decoder(payload, metadata) {
	"use strict";

	var msg = JSON.parse(String.fromCharCode.apply(String, payload));

	var result = {
		deviceType: "pzemMtr",
		groupName: "transformers",
		deviceName: metadata.deviceName,
		customerName: metadata.customerName,
		attributes: {
			nodeId: Number(metadata.nodeId)
		},
		telemetry: {
			ts: Date.now(),
			values: {}
		}
	};
	var blocks = [],
		i = 0,
		len = Array.isArray(msg.forecasted_blocks) ? msg.forecasted_blocks.length : 0,
		block;
	for (i = 0; i < len; i += 1) {
		block = msg.forecasted_blocks[i];
		blocks.push({
			startTime: block.start_time,
			endTime: block.end_time,
			value: block.capacity,
			unit: block.unit,
			forecastIdentifier: metadata["Header:x-request-id"]
		});
	}
	if (msg.type === "CONSUMPTION") {
		result.telemetry.values.oscpCapProfileCons = blocks;
	} else if (msg.type === "GENERATION") {
		result.telemetry.values.oscpCapProfileGen = blocks;
	} else if (msg.type === "FALLBACK_CONSUMPTION") {
		result.telemetry.values.oscpCapProfileFbCons = blocks;
	} else if (msg.type === "FALLBACK_GENERATION") {
		result.telemetry.values.oscpCapProfileFbGen = blocks;
	} else if (msg.type === "OPTIMUM") {
		result.telemetry.values.oscpCapProfileOpt = blocks;
	}

	return result;
}
