/* NOTE that for TB integration, the first "export ... {" line and final "}" line
   are NOT used; only the BODY of this function should be copy/pasted into TB.

   The supported topic patterns supported by this converter are:

   OSCP Heartbeat: user/1/node/2/datum/0/oscp/NZBus/pnmr_trans_1/cp/Vector/NZ-AUK_0001/h
   OSCP UGCF:      user/1/node/2/datum/0/oscp/NZBus/pnmr_trans_1/cp/Vector/NZ-AUK_0001/ugcf/c
   Charger:        user/1/node/2/datum/0/nzbus/pnmr/chgr/1
   Transformer:    user/1/node/2/datum/0/nzbus/pnmr/trans/1
   PSU:            user/1/node/2/datum/0/nzbus/pnmr/psu
*/

/**
 * @typedef OscpSource
 * @type {object}
 * @property {string} topic the full MQTT topic
 * @property {number} userId the SolarNetwork user ID
 * @property {number} nodeId the SolarNetwork node ID
 * @property {string} aggregation the datum aggregation key
 * @property {string} sourceId the datum stream source ID
 * @property {string} customerName the TB customer name
 * @property {string} deviceName the TB device name
 * @property {string} capacityProviderName the SolarNetwork OSCP Capacity Provider name
 * @property {string} capacityGroupIdentifier the SolarNetwork OSCP Capacity Group identifier
 * @property {string} actioncode the OSCP action name, as a shorthand code
 * @property {string} type an extra type qualifier, e.g. for UpdateGroupCapacityForecast messages
 */

/**
 * @typedef PlcSource
 * @type {object}
 * @property {string} topic the full MQTT topic
 * @property {number} userId the SolarNetwork user ID
 * @property {number} nodeId the SolarNetwork node ID
 * @property {string} aggregation the datum aggregation key
 * @property {string} sourceId the datum stream source ID
 * @property {string} customerName the TB customer name
 * @property {string} siteName the TB site name
 * @property {string} deviceType the TB device type
 * @property {number} deviceNumber the TB device number
 */

export default function Decoder(payload, metadata) {
	"use strict";
	// CBOR decoding adapted from https://github.com/ARMmbed/cbor-sync
	// Copyright (c) 2014 ARM Ltd.
	// Released under MIT license.

	var CBOR = (function() {
		var semanticDecoders = {};

		var notImplemented = function(label) {
			return function() {
				throw new Error(label + " not implemented");
			};
		};

		/* utf.js - UTF-8 <=> UTF-16 convertion
		 *
		 * Copyright (C) 1999 Masanao Izumo <iz@onicos.co.jp>
		 * Version: 1.0
		 * LastModified: Dec 25 1999
		 * This library is free.  You can redistribute it and/or modify it.
		 */
		function utf8ArrayToStr(array) {
			var out, i, len, c;
			var char2, char3;

			out = "";
			len = array.length;
			i = 0;
			while (i < len) {
				c = array[i++];
				switch (c >> 4) {
					case 0:
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
						// 0xxxxxxx
						out += String.fromCharCode(c);
						break;
					case 12:
					case 13:
						// 110x xxxx   10xx xxxx
						char2 = array[i++];
						out += String.fromCharCode(((c & 0x1f) << 6) | (char2 & 0x3f));
						break;
					case 14:
						// 1110 xxxx  10xx xxxx  10xx xxxx
						char2 = array[i++];
						char3 = array[i++];
						out += String.fromCharCode(
							((c & 0x0f) << 12) | ((char2 & 0x3f) << 6) | ((char3 & 0x3f) << 0)
						);
						break;
				}
			}
			return out;
		}
		function Reader() {}
		Reader.prototype = {
			peekByte: notImplemented("peekByte"),
			readByte: notImplemented("readByte"),
			readChunk: notImplemented("readChunk"),
			readFloat16: function() {
				var half = this.readUint16();
				var exponent = (half & 0x7fff) >> 10;
				var mantissa = half & 0x3ff;
				var negative = half & 0x8000;
				if (exponent === 0x1f) {
					if (mantissa === 0) {
						return negative ? -Infinity : Infinity;
					}
					return NaN;
				}
				var magnitude = exponent
					? Math.pow(2, exponent - 25) * (1024 + mantissa)
					: Math.pow(2, -24) * mantissa;
				return negative ? -magnitude : magnitude;
			},
			readFloat32: function() {
				var intValue = this.readUint32();
				var exponent = (intValue & 0x7fffffff) >> 23;
				var mantissa = intValue & 0x7fffff;
				var negative = intValue & 0x80000000;
				if (exponent === 0xff) {
					if (mantissa === 0) {
						return negative ? -Infinity : Infinity;
					}
					return NaN;
				}
				var magnitude = exponent
					? Math.pow(2, exponent - 23 - 127) * (8388608 + mantissa)
					: Math.pow(2, -23 - 126) * mantissa;
				return negative ? -magnitude : magnitude;
			},
			readFloat64: function() {
				var int1 = this.readUint32(),
					int2 = this.readUint32();
				var exponent = (int1 >> 20) & 0x7ff;
				var mantissa = (int1 & 0xfffff) * 4294967296 + int2;
				var negative = int1 & 0x80000000;
				if (exponent === 0x7ff) {
					if (mantissa === 0) {
						return negative ? -Infinity : Infinity;
					}
					return NaN;
				}
				var magnitude = exponent
					? Math.pow(2, exponent - 52 - 1023) * (4503599627370496 + mantissa)
					: Math.pow(2, -52 - 1022) * mantissa;
				return negative ? -magnitude : magnitude;
			},
			readUint16: function() {
				return this.readByte() * 256 + this.readByte();
			},
			readUint32: function() {
				return this.readUint16() * 65536 + this.readUint16();
			},
			readUint64: function() {
				return this.readUint32() * 4294967296 + this.readUint32();
			}
		};
		function ArrayReader(data) {
			this.$data = data;
			this.pos = 0;
		}
		ArrayReader.prototype = Object.create(Reader.prototype);
		ArrayReader.prototype.peekByte = function() {
			return this.$data[this.pos];
		};
		ArrayReader.prototype.readByte = function() {
			return this.$data[this.pos++];
		};
		ArrayReader.prototype.readChunk = function(length) {
			var result = this.$data.slice(this.pos, this.pos + length);
			this.pos += length;
			return result;
		};

		function valueFromHeader(value, reader) {
			if (value < 24) {
				return value;
			} else if (value == 24) {
				return reader.readByte();
			} else if (value == 25) {
				return reader.readUint16();
			} else if (value == 26) {
				return reader.readUint32();
			} else if (value == 27) {
				return reader.readUint64();
			} else if (value == 31) {
				// special value for non-terminating arrays/objects
				return null;
			}
			notImplemented("Additional info: " + value)();
		}

		var stopCode = new Error(); // Just a unique object, that won't compare strictly equal to anything else

		function decodeArray(type, value, reader) {
			var arrayLength = valueFromHeader(value, reader),
				result = [],
				objResult,
				i,
				item;
			if (arrayLength !== null) {
				if (type === 5) {
					arrayLength *= 2;
				}
				for (i = 0; i < arrayLength; i++) {
					result[i] = decodeReader(reader);
				}
			} else {
				item;
				while ((item = decodeReader(reader)) !== stopCode) {
					result.push(item);
				}
			}
			if (type === 5) {
				objResult = {};
				for (i = 0; i < result.length; i += 2) {
					objResult[result[i]] = result[i + 1];
				}
				return objResult;
			}
			return result;
		}

		function decodeTaggedValue(value, reader) {
			var tag = valueFromHeader(value, reader);
			var decoder = semanticDecoders[tag];
			var result = decodeReader(reader);
			return decoder ? decoder(result) : result;
		}

		function decodeFloatingPoint(value, reader) {
			if (value === 25) {
				return reader.readFloat16();
			} else if (value === 26) {
				return reader.readFloat32();
			} else if (value === 27) {
				return reader.readFloat64();
			}
			switch (valueFromHeader(value, reader)) {
				case 20:
					return false;
				case 21:
					return true;
				case 22:
					return null;
				case 23:
					return undefined;
				case null:
					return stopCode;
				default:
					throw new Error("Unknown fixed value: " + value);
			}
		}

		function decodeReader(reader) {
			var firstByte = reader.readByte(),
				type = firstByte >> 5,
				value = firstByte & 0x1f;
			switch (type) {
				case 0:
					return valueFromHeader(value, reader);
				case 1:
					return -1 - valueFromHeader(value, reader);
				case 2:
					return reader.readChunk(valueFromHeader(value, reader));
				case 3:
					return utf8ArrayToStr(reader.readChunk(valueFromHeader(value, reader)));
				case 4:
				case 5:
					return decodeArray(type, value, reader);
				case 6:
					return decodeTaggedValue(value, reader);
				case 7:
					return decodeFloatingPoint(value, reader);
				default:
					throw new Error("Unsupported type/vaue: " + type + "/" + value);
			}
		}

		var api = {
			decode: function(data) {
				var reader = new ArrayReader(data);
				return decodeReader(reader);
			},
			addSemanticDecode: function(tag, fn) {
				if (typeof tag !== "number" || tag % 1 !== 0 || tag < 0) {
					throw new Error("Tag must be a positive integer");
				}
				semanticDecoders[tag] = fn;
				return this;
			},
			Reader: Reader
		};

		api.addSemanticDecode(0, function(isoString) {
			return new Date(isoString);
		})
			.addSemanticDecode(1, function(isoString) {
				return new Date(isoString);
			})
			.addSemanticDecode(4, function(data) {
				// handle decimal floats, which arrive as array of 2 elements; https://tools.ietf.org/html/rfc7049#section-2.4.3
				var e;
				if (Array.isArray(data) && data.length > 1) {
					e = data[0];
					return data[1] * Math.pow(10, e);
				}
				return data;
			});

		return api;
	})();

	var resultTemplate = {
		deviceType: "pzemMtr",
		groupName: "transformers"
	};

	/**
	 * Extract OSCP source compoennts from a MQTT message.
	 *
	 * General MQTT topic structure is like
	 *
	 * user/{userId}/node/{nodeId}/datum/{agg}/oscp/{customerName}/{deviceName}/{role}/{cpName}/{cgIdentifier}/{actionCode}(/{type})
	 *
	 * where {role} is "cp" for Capacity Provider, and {action} is one of `h` (Heartbeat) or `ugcf` (UpdateGroupCapacityForecast)
	 * and {type} is used only in UpdateGroupCapacityForecast messages.
	 *
	 * The `m` regex match result contains 10 capture groups taken from the above
	 * parameters, minus the {role} parameter which is assumed to be "cp".
	 *
	 * @param {object} metadata the Thingsboard metadata
	 * @param {string} metadata.topic the MQTT message topic
	 * @returns {OscpSource} the source components with the message
	 */
	function oscpSource(metadata) {
		/** @type {string} */
		var topic = (metadata ? metadata.topic : undefined);
		if (!topic) {
			return undefined;
		}
		var m = /user\/(\d+)\/node\/(\d+)\/datum\/([^/]+)\/(oscp\/([^/]+)\/([^/]+)\/cp\/([^/]+)\/([^/]+)\/([^/]+)(?:\/([^/]+))?)/.exec(
			topic
		);
		if (!(m && m.length > 0)) {
			return undefined;
		}
		var result = {
			topic: topic,
			userId: Number(m[1]),
			nodeId: Number(m[2]),
			aggregation: m[3],
			sourceId: m[4],
			customerName: m[5],
			deviceName: m[6],
			capacityProviderName: m[7],
			capacityGroupIdentifier: m[8],
			action: m[9]
		};
		if (m[10]) {
			result.type = m[10];
		}
		return result;
	}

	/**
	 * Extract PLC source components from a MQTT message.
	 *
	 * General MQTT topic structure is like
	 *
	 * user/{userId}/node/{nodeId}/datum/{agg}/{customerName}/{siteName}/{deviceType}(/{num})
	 *
	 * The `m` regex match result contains 8 capture groups taken from the above
	 * parameters.
	 *
	 * @param {object} metadata the Thingsboard metadata
	 * @param {string} metadata.topic the MQTT message topic
	 * @returns {PlcSource} the source components with the message
	 */
	function plcSource(metadata) {
		/** @type {string} */
		var topic = (metadata ? metadata.topic : undefined);
		if (!topic) {
			return undefined;
		}
		var m = /user\/(\d+)\/node\/(\d+)\/datum\/([^/]+)\/(([^/]+)\/([^/]+)\/([^/]+)(?:\/(\d+))?)/.exec(
			topic
		);
		if (!(m && m.length > 0)) {
			return undefined;
		}
		var result = {
			topic: topic,
			userId: Number(m[1]),
			nodeId: Number(m[2]),
			aggregation: m[3],
			sourceId: m[4],
			customerName: m[5],
			siteName: m[6],
			deviceType: m[7]
		};
		if (m[8] !== undefined) {
			result.deviceNumber = Number(m[8]);
		}
		return result;
	}

	/**
	 * Extract a Heartbeat message result.
	 *
	 * Heartbeat messages are in the form:
	 *
	 * ```json
	 * {
	 * 	  "created" : 1667870054145,
	 * 	  "expires" : "2022-11-08 03:14:10Z",
	 * 	  "nodeId" : 612,
	 * 	  "sourceId" : "/oscp/nzbus/pnmt_trans_1/cp/Vector/NZ-AUK_0001/h"
	 * }
	 * ```
	 *
	 * @param {object} datum the datum
	 * @param {OscpSource} source the source components
	 * @returns {object} a Thingsboard event object
	 */
	function extractHeartbeat(datum, source) {
		return Object.assign(
			{
				deviceName: source.deviceName,
				customerName: source.customerName,
				attributes: {
					nodeId: source.nodeId,
					cpOscpExpiry: datum.expires
				}
			},
			resultTemplate
		);
	}

	/**
	 * Extract a UpdateGroupCapacityForecast message result.
	 *
	 * UpdateGroupCapacityForecast messages are in the form:
	 *
	 * ```json
	 * {
	 * 	  "amount" : 1000.0,
	 * 	  "created" : 1667887099000,
	 * 	  "duration" : 0,
	 * 	  "forecastIdentifier" : "tu26s1KhMlNUeoG3wVP77tn5JcdiK2NL",
	 * 	  "nodeId" : 612,
	 * 	  "sourceId" : "/oscp/nzbus/pnmr_trans_1/cp/Vector/NZ-AUK_0001/ugcf/c",
	 * 	  "unit" : "kW"
	 * }
	 * ```
	 *
	 * @param {object} datum the datum
	 * @param {OscpSource} source the source components
	 * @returns {object} a Thingsboard event object
	 */
	function extractForecast(datum, source) {
		return Object.assign(
			{
				deviceName: source.deviceName,
				customerName: source.customerName,
				attributes: {
					nodeId: source.nodeId
				},
				telemetry: {
					ts: datum.created,
					values: {
						endTime: datum.created + datum.duration * 1000,
						value: datum.amount,
						unit: datum.unit,
						forecastIdentifier: datum.forecastIdentifier,
						forecastType: source.type
					}
				}
			},
			resultTemplate
		);
	}

	/**
	 * Extract a PLC datum message result.
	 *
	 * PLC messages are in the form:
	 *
	 * ```json
	 * {
	 * 	  "created" : 1667887099000,
	 * 	  "cbStatus": 0,
	 * 	  "cbFault": 0,
	 * 	  "cbPower": 1,
	 * 	  "lmpAvail": 1,
	 * 	  "lmpUnavail": 0
	 * }
	 * ```
	 *
	 * @param {object} datum the datum
	 * @param {PlcSource} source the source components
	 * @param {object} metadata the input metadata
	 * @returns {object} a Thingsboard event object
	 */
	function extractPlcDatum(datum, source, metadata) {
		var value = {};
		var prop;
		for (prop in datum) {
			if (prop.startsWith("_") || prop === "sourceId" || prop === "created") {
				continue;
			}
			value[prop] = datum[prop];
		}
		var deviceName =
			source.deviceType === "chgr"
				? source.siteName +
				  source.deviceType.substring(0, 1).toUpperCase() +
				  source.deviceType.substring(1) +
				  source.deviceNumber
				: metadata[source.deviceType + "DeviceName"];
		return {
			groupName: metadata[source.deviceType + "DeviceGroup"],
			deviceType: metadata[source.deviceType + "DeviceType"],
			deviceName: deviceName,
			customerName: metadata.customer,
			attributes: {
				nodeId: source.nodeId
			},
			telemetry: {
				ts: datum.created,
				values: value
			}
		};
	}

	/** Main entry point **/

	if (!metadata || !metadata.topic) {
		return [];
	}

	var result;
	var datum = CBOR.decode(payload);
	var source = oscpSource(metadata);

	if (source) {
		if (source.action === "h") {
			result = extractHeartbeat(datum, source);
		} else if (source.action === "ugcf") {
			result = extractForecast(datum, source);
		}
	} else {
		source = plcSource(metadata);
		if (source) {
			result = extractPlcDatum(datum, source, metadata);
		}
	}

	return result;
}
