// NOTE that for TB integration, the first "export ... {" line and final "}" line
//      are NOT used; only the BODY of this function should be copy/pasted into TB.
export default function Decoder(payload, metadata) {
	"use strict";
	// CBOR decoding adapted from https://github.com/ARMmbed/cbor-sync
	// Copyright (c) 2014 ARM Ltd.
	// Released under MIT license.

	var CBOR = (function() {
		var semanticDecoders = {};

		var notImplemented = function(label) {
			return function() {
				throw new Error(label + " not implemented");
			};
		};

		/* utf.js - UTF-8 <=> UTF-16 convertion
		 *
		 * Copyright (C) 1999 Masanao Izumo <iz@onicos.co.jp>
		 * Version: 1.0
		 * LastModified: Dec 25 1999
		 * This library is free.  You can redistribute it and/or modify it.
		 */
		function utf8ArrayToStr(array) {
			var out, i, len, c;
			var char2, char3;

			out = "";
			len = array.length;
			i = 0;
			while (i < len) {
				c = array[i++];
				switch (c >> 4) {
					case 0:
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
						// 0xxxxxxx
						out += String.fromCharCode(c);
						break;
					case 12:
					case 13:
						// 110x xxxx   10xx xxxx
						char2 = array[i++];
						out += String.fromCharCode(((c & 0x1f) << 6) | (char2 & 0x3f));
						break;
					case 14:
						// 1110 xxxx  10xx xxxx  10xx xxxx
						char2 = array[i++];
						char3 = array[i++];
						out += String.fromCharCode(
							((c & 0x0f) << 12) | ((char2 & 0x3f) << 6) | ((char3 & 0x3f) << 0)
						);
						break;
				}
			}
			return out;
		}
		function Reader() {}
		Reader.prototype = {
			peekByte: notImplemented("peekByte"),
			readByte: notImplemented("readByte"),
			readChunk: notImplemented("readChunk"),
			readFloat16: function() {
				var half = this.readUint16();
				var exponent = (half & 0x7fff) >> 10;
				var mantissa = half & 0x3ff;
				var negative = half & 0x8000;
				if (exponent === 0x1f) {
					if (mantissa === 0) {
						return negative ? -Infinity : Infinity;
					}
					return NaN;
				}
				var magnitude = exponent
					? Math.pow(2, exponent - 25) * (1024 + mantissa)
					: Math.pow(2, -24) * mantissa;
				return negative ? -magnitude : magnitude;
			},
			readFloat32: function() {
				var intValue = this.readUint32();
				var exponent = (intValue & 0x7fffffff) >> 23;
				var mantissa = intValue & 0x7fffff;
				var negative = intValue & 0x80000000;
				if (exponent === 0xff) {
					if (mantissa === 0) {
						return negative ? -Infinity : Infinity;
					}
					return NaN;
				}
				var magnitude = exponent
					? Math.pow(2, exponent - 23 - 127) * (8388608 + mantissa)
					: Math.pow(2, -23 - 126) * mantissa;
				return negative ? -magnitude : magnitude;
			},
			readFloat64: function() {
				var int1 = this.readUint32(),
					int2 = this.readUint32();
				var exponent = (int1 >> 20) & 0x7ff;
				var mantissa = (int1 & 0xfffff) * 4294967296 + int2;
				var negative = int1 & 0x80000000;
				if (exponent === 0x7ff) {
					if (mantissa === 0) {
						return negative ? -Infinity : Infinity;
					}
					return NaN;
				}
				var magnitude = exponent
					? Math.pow(2, exponent - 52 - 1023) * (4503599627370496 + mantissa)
					: Math.pow(2, -52 - 1022) * mantissa;
				return negative ? -magnitude : magnitude;
			},
			readUint16: function() {
				return this.readByte() * 256 + this.readByte();
			},
			readUint32: function() {
				return this.readUint16() * 65536 + this.readUint16();
			},
			readUint64: function() {
				return this.readUint32() * 4294967296 + this.readUint32();
			}
		};
		function ArrayReader(data) {
			this.$data = data;
			this.pos = 0;
		}
		ArrayReader.prototype = Object.create(Reader.prototype);
		ArrayReader.prototype.peekByte = function() {
			return this.$data[this.pos];
		};
		ArrayReader.prototype.readByte = function() {
			return this.$data[this.pos++];
		};
		ArrayReader.prototype.readChunk = function(length) {
			var result = this.$data.slice(this.pos, this.pos + length);
			this.pos += length;
			return result;
		};

		function valueFromHeader(value, reader) {
			if (value < 24) {
				return value;
			} else if (value == 24) {
				return reader.readByte();
			} else if (value == 25) {
				return reader.readUint16();
			} else if (value == 26) {
				return reader.readUint32();
			} else if (value == 27) {
				return reader.readUint64();
			} else if (value == 31) {
				// special value for non-terminating arrays/objects
				return null;
			}
			notImplemented("Additional info: " + value)();
		}

		var stopCode = new Error(); // Just a unique object, that won't compare strictly equal to anything else

		function decodeArray(type, value, reader) {
			var arrayLength = valueFromHeader(value, reader),
				result = [],
				objResult,
				i,
				item;
			if (arrayLength !== null) {
				if (type === 5) {
					arrayLength *= 2;
				}
				for (i = 0; i < arrayLength; i++) {
					result[i] = decodeReader(reader);
				}
			} else {
				item;
				while ((item = decodeReader(reader)) !== stopCode) {
					result.push(item);
				}
			}
			if (type === 5) {
				objResult = {};
				for (i = 0; i < result.length; i += 2) {
					objResult[result[i]] = result[i + 1];
				}
				return objResult;
			}
			return result;
		}

		function decodeTaggedValue(value, reader) {
			var tag = valueFromHeader(value, reader);
			var decoder = semanticDecoders[tag];
			var result = decodeReader(reader);
			return decoder ? decoder(result) : result;
		}

		function decodeFloatingPoint(value, reader) {
			if (value === 25) {
				return reader.readFloat16();
			} else if (value === 26) {
				return reader.readFloat32();
			} else if (value === 27) {
				return reader.readFloat64();
			}
			switch (valueFromHeader(value, reader)) {
				case 20:
					return false;
				case 21:
					return true;
				case 22:
					return null;
				case 23:
					return undefined;
				case null:
					return stopCode;
				default:
					throw new Error("Unknown fixed value: " + value);
			}
		}

		function decodeReader(reader) {
			var firstByte = reader.readByte(),
				type = firstByte >> 5,
				value = firstByte & 0x1f;
			switch (type) {
				case 0:
					return valueFromHeader(value, reader);
				case 1:
					return -1 - valueFromHeader(value, reader);
				case 2:
					return reader.readChunk(valueFromHeader(value, reader));
				case 3:
					return utf8ArrayToStr(reader.readChunk(valueFromHeader(value, reader)));
				case 4:
				case 5:
					return decodeArray(type, value, reader);
				case 6:
					return decodeTaggedValue(value, reader);
				case 7:
					return decodeFloatingPoint(value, reader);
				default:
					throw new Error("Unsupported type/vaue: " + type + "/" + value);
			}
		}

		var api = {
			decode: function(data) {
				var reader = new ArrayReader(data);
				return decodeReader(reader);
			},
			addSemanticDecode: function(tag, fn) {
				if (typeof tag !== "number" || tag % 1 !== 0 || tag < 0) {
					throw new Error("Tag must be a positive integer");
				}
				semanticDecoders[tag] = fn;
				return this;
			},
			Reader: Reader
		};

		api.addSemanticDecode(0, function(isoString) {
			return new Date(isoString);
		})
			.addSemanticDecode(1, function(isoString) {
				return new Date(isoString);
			})
			.addSemanticDecode(4, function(data) {
				// handle decimal floats, which arrive as array of 2 elements; https://tools.ietf.org/html/rfc7049#section-2.4.3
				var e;
				if (Array.isArray(data) && data.length > 1) {
					e = data[0];
					return data[1] * Math.pow(10, e);
				}
				return data;
			});

		return api;
	})();

	/** Helper functions **/

	/**
	 * Convert a datum object into a telemetry object.
	 *
	 * @param {Object} datum  the datum object to convert to a telemetry object
	 */
	function datumToTelemetry(datum) {
		var prop,
			values = {},
			result = { values: values };
		if (datum) {
			for (prop in datum) {
				if (prop.startsWith("_") || prop == "sourceId") {
					// skip props starting with _ like _DatumType;
					// skip sourceId as that is the device name
					continue;
				} else if (prop == "created") {
					// created date stored separately
					result.ts = datum[prop];
				} else {
					values["ocpp_" + prop] = datum[prop];
					if (prop == "status" || prop == "wattHours" || prop == "watts") {
						values[prop] = datum[prop];
					} else if (prop == "voltage_a") {
						values.voltage = datum[prop];
					} else if (prop == "current_a") {
						values.current = datum[prop];
					} else if (prop == "sessionId") {
						values.chargeSessionId = datum[prop];
					}
				}
			}
		}
		return result;
	}

	function datumToAttributes(datum, metadata) {
		if (!(metadata && metadata.topic)) {
			return undefined;
		}
		var nodeId, chargerIdent;
		var m = /\/node\/(\d+)\//.exec(metadata.topic);
		if (!(m && m.length > 0)) {
			return undefined;
		}
		nodeId = Number(m[1]);
		m = /\/cp\/([^/]+)\//.exec(metadata.topic);
		if (!(m && m.length > 0)) {
			return undefined;
		}
		chargerIdent = m[1];
		return {
			nodeId: nodeId,
			chargerIdentifier: chargerIdent
		};
	}

	function datumToSourceId(datum, metadata) {
		if (metadata && metadata.topic) {
			// extract from topic
			var m = /datum\/[0-9a-zA-Z]+\/(.+)$/.exec(metadata.topic);
			if (m && m.length > 0) {
				return m[1];
			}
		}
		return datum ? datum.sourceId : undefined;
	}

	/** Main entry point **/

	// decode payload to JSON datum
	var datum = CBOR.decode(payload);
	var sourceId = datumToSourceId(datum, metadata);
	var sourceComponents = sourceId ? sourceId.replace(/(^\/+|\/+$)/g, "").split("/") : undefined;
	if (!(Array.isArray(sourceComponents) && sourceComponents.length > 3)) {
		return [];
	}
	var telem = datumToTelemetry(datum);
	var attr = datumToAttributes(datum, metadata);
	// if topic = user/504/node/449/datum/0/NZBus/Panmure/ocpp/cp/JAJA/1
	var connectorId = sourceComponents[5]; // sourceComponents[5] will be 1, sourceComponents[4] will be JAJA
	var result;
	if (connectorId > 0) {
		result = {
			deviceName: sourceComponents[4] + "_cn" + sourceComponents[5],
			deviceType: "Sinexcel",
			customerName: sourceComponents[0],
			groupName: "connector",
			telemetry: telem
		};
	} else {
		result = {
			deviceName: sourceComponents[4],
			deviceType: "SinexcelChg",
			customerName: sourceComponents[0],
			groupName: "charger",
			telemetry: telem
		};
	}
	if (attr) {
		result.attributes = attr;
	}
	return result;
}
